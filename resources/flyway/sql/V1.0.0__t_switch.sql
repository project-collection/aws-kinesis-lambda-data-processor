CREATE TABLE  IF NOT EXISTS semon_core."t_switch" (
	"data_mac_address" VARCHAR PRIMARY KEY,
	"verbundkennung" VARCHAR,
	"data_ip" VARCHAR,
	"data_management_address" VARCHAR,
	"data_timestamp" TIMESTAMP WITH TIME ZONE
);