# NWM-Transform-Stream
 
[[_TOC_]]
 
 
## Kurzbeschreibung

### Architektur

![architecture](./docs/architecture.png)


### Inhalt & Ablauf

NWM data come into landing zone every **5** minutes and ingested into firehose by ingestion lambda, after arriving in kinesis data stream (flake-nwm-data-stream) will be processed by this repository.

This repository is built according to current FLAKE STANDARD STREAMING ARCHITECTURE (ingestion kinesis data stream -> processed lambda -> processed kinesis data stream -> store lambda -> target database).

1. processed and store lambda

**Processed lambda** and **store lambda** are configured to use configuration file [here](src/nwm_transform_stream/resources/conf/), namely kinesis_batch_size, kinesis_batch_window etc can be adjusted according to the use case and incoming data

In **processed lambda** following transformation is implemented:

* validate if the schema is correct or not (only type switch is accepted here), not validated record will be marked as corrupted -> ignored -> number of corrupted records logged into cloudwatch metrics
* transformation according to business logic, function **transform_switch**
* write to processed kinesis data stream

lambda powertool batch processing is activated on **processed lambda** , so it can handle partially successful event, only when all records failed to be processed, a BatchProcessingError exception will be thrown


2. Cloudwatch Metrics

Cloudwatch metrics are implemented with lambda powertools, [following metrics](src/nwm_transform_stream/common/config.py) are collected and visualized in monitoring dashboard

* InputRecordsCountSwitchProcessed
* OutputRecordsCountSwitchProcessed
* InputputRecordsCountSwitchStore
* InvalidRecordsCountSwitchProcessed

the schema is MetricName+NWMUseCase+(ProcessedStore), additionally two extra cloudwatch metrics are also loaded from flake python commons library

* RowsWritten
* DurationSeconds

3. schema

json schema w.r.t records in kinesis located in [here](src/nwm_transform_stream/resources/schema/) coming from nwm-transform batch process [resources](https://git.tech.rz.db.de/ppds/dataflows/flake/nwm/nwm-transform/-/tree/master/src/main/resources/conf/curated/json_schema). The json files with original is the same as in nwm-transform, but they do not match the incoming data, so they are adapted to streaming case (for example, switch-v1.6.1-original.json -> switch-v1.6.1.json)

### How to Add a new Record Schema

The json schema for different types of data (now **ONLY** switch) changes along the time (for example, v1.6.1 -> v1.9.1), here are the steps suggested to add new schema

1. put json schema of the new version into [here](./src/nwm_transform_stream/resources/schema/), following naming convention {type}-v{version}.json (for example, switch-v1.6.1.json)
2. test data in kinesis with this new version, see if they match, sometime they DON'T IN NWM case
3. (optional) if the delivered schema does not match incoming record, then rename the delivered schema with **original** suffix, and adapt that schema store it under name {type}-v{version}.json
4. (optinoal) adding unit tests accordingly

> **Note:** only schema with naming convention {type}-v{version}.json will be used by validate_schema function to validate the incoming records


### Links
 
- [Fachliche Beschreibung](https://dbsystel.wiki.intranet.deutschebahn.com/wiki/x/LYZNGg)
- [Monitoring Dashboard](https://monitoring.flake.comp.db.de/dashboards)
- [Runbook-Eintrag](https://ppds.gitpages.tech.rz.db.de/infrastructure/ppds-runbook/doc/public/04-runbook/00-runbook.html#netzwerkmanager-transform-stream)


## Weiterentwicklung
 
### Entwicklungsumgebung
 
For further development of this project, you need to install poetry and correct version of python on your machine and initalize the poetry virtual environment with

```bash
poetry install
```

and get into the environment

```bash
poetry shell
``` 

Now start to develop, for example run unit test with

```bash
# you can also ignore poetry shell, using poetry run pytest directly within poetry virtual environment 
pytest tests/unit
``` 

Notice: since this repository include flake specific python package, you need to have Jforg Artifactory credentials configured with

```bash
poetry config http-basic.db_pypi $ARTIFACTORY_USER $ARTIFACTORY_PW
poetry config http-basic.db_cdkdbs $ARTIFACTORY_USER $ARTIFACTORY_PW
poetry config http-basic.flake_dev $ARTIFACTORY_USER $ARTIFACTORY_PW
poetry config http-basic.flake_prod $ARTIFACTORY_USER $ARTIFACTORY_PW
poetry config http-basic.default $ARTIFACTORY_USER $ARTIFACTORY_PW
```

here $ARTIFACTORY_USER and $ARTIFACTORY_PW is the username and password of your personal Jforg Artifactory account


### Lambda Powertool

many features of lambda powertool (https://github.com/aws-powertools/powertools-lambda-python) are used in the process of lambda function development

* batch processing
* metrics
* event source
* data model

## Besonderheiten
 
* [switch schema v1.9.1](src/nwm_transform_stream/resources/schema/switch-v1.9.1-original.json) does not completely match the incoming kinesis payload, namely no managementAddress and forwardDatabaseTable field is present (and does not influence the t_switch table use), so the original schema is customized to [new version](src/nwm_transform_stream/resources/schema/switch-v1.9.1.json) (without the above two fields as **required**)


## Bekannte Probleme & Lösungen
 
* There are at least 3 different record types in kinesis stream, com.deutschebahn.dbfv.nwm.switch ([example data](./tests/resource/switch_valid_kds_record_v1.9.1.json)), com.deutschebahn.dbfv.nwm.mapping ([example data](./tests/resource/switch_invalid_kds_record_v1.9.1_1.json)) and com.deutschebahn.dbfv.nwm.ntop ([example data](./tests/resource/switch_invalid_kds_record_v1.9.1_2.json)), should we split them into different prefixes by ingestion ?
