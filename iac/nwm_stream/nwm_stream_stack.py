import sys
import os

sys.path.append("../src/")

from ppds_cdk_lib.common.construct.lambda_function import GenericLambda
from aws_cdk import aws_lambda, aws_iam, aws_lambda_event_sources, aws_kinesis, Duration
from ppds_cdk_lib.common.construct.lambda_role import GenericLambdaRole
from ppds_cdk_lib.common.application_context import ApplicationContext
from ppds_cdk_lib.flake.flake_stream_stack import FlakeStreamStack
from ppds_cdk_lib.common.construct.kinesis_stream import GenericKinesisStream
from nwm_transform_stream.processed.config import ProcessedConfig
from nwm_transform_stream.store.config import StoreConfig
from nwm_transform_stream.common.utils import load_config


env = os.environ["CI_ENVIRONMENT_NAME"]
CI_COMMIT_SHORT_SHA = os.environ["CI_COMMIT_SHORT_SHA"].lower()

PROCESSED_CONFIG_PATH = (
    f"../src/nwm_transform_stream/resources/conf/processed/{env.lower()}.conf"
)
STORE_CONFIG_PATH = (
    f"../src/nwm_transform_stream/resources/conf/store/{env.lower()}.conf"
)


class NWMTransformStreamStack(FlakeStreamStack):

    PROJECT_SLUG = "nwm"

    def __init__(
        self, scope, app_ctx: ApplicationContext, cost_monitoring_tag: str, **kwargs
    ) -> None:
        super().__init__(scope, app_ctx, cost_monitoring_tag, **kwargs)
        self.template_options.template_format_version = "2010-09-09"

        self._get_secrets_manager()

        # Load config files to determine configured values
        self.processed_config: ProcessedConfig = load_config(
            env=app_ctx.env_name.value,
            path=PROCESSED_CONFIG_PATH,
            config_type=ProcessedConfig,
        )
        self.store_config: StoreConfig = load_config(
            env=app_ctx.env_name.value, path=STORE_CONFIG_PATH, config_type=StoreConfig
        )

        # get kinesis stream name
        switch_kinesis_input_arn = self.source_kinesis_arns[self.app_ctx.env_name.value]
        switch_kinesis_output: aws_kinesis.Stream = self._create_processed_kinesis(
            "switch"
        )

        self._create_processed_lambda(
            source_kinesis_stream_arn=switch_kinesis_input_arn,
            destination_kinesis_stream_arn=switch_kinesis_output.stream_arn,
            usecase_name="switch",
        )

        self._create_store_lambda(
            source_kinesis_stream_arn=switch_kinesis_output.stream_arn,
            usecase_name="switch",
        )

    def _create_processed_kinesis(self, usecase_name: str) -> aws_kinesis.Stream:
        """
        Create a kinesis stream for processed data with two shards and a retention of 7 days

        :param usecase_name: The name of the usecase, e.g wanlinks or system1 etc
        :type usecase_name: str
        :return: The created datastream
        :rtype: aws_kinesis.Stream
        """
        return GenericKinesisStream(
            self, self.app_ctx, self.kms, usecase_name
        ).get_data_stream(
            name=f"{usecase_name}_processed_kinesis",
            retention_period=168,
            shard_count=2,
        )

    def _create_processed_lambda(
        self,
        source_kinesis_stream_arn: str,
        usecase_name: str,
        destination_kinesis_stream_arn: str = None,
    ) -> aws_lambda.Function:
        """
        Creates a processing lambda function instance which processes the
        incoming kinesis data and sends it to the processed kinesis stream

        :param source_kinesis_stream_arn: arn of the incoming kinesis stream
        :type source_kinesis_stream_arn: str
        :param destination_kinesis_stream_arn: arn of the kinesis stream which gets data from lambda, defaults to None
        :type destination_kinesis_stream_arn: str, optional
        :param usecase_name: name of the usecase, e.g. wanlink, system1 etc, defaults to "usecase"
        :type usecase_name: str, optional
        :return: the created lambda function
        :rtype: aws_lambda.Function
        """

        lambda_role: aws_iam.Role = GenericLambdaRole(
            self, self.app_ctx, f"{usecase_name}-processed-role"
        ).create_lambda_role(
            name=f"{usecase_name}_processed",
            source_kinesis_stream_arn=source_kinesis_stream_arn,
            destinantion_kinesis_stream_arn=destination_kinesis_stream_arn,
        )
        GenericLambdaRole.add_kinesis_src(
            role=lambda_role, source_kinesis_stream_arn=source_kinesis_stream_arn
        )

        # create lambda
        lambda_function: aws_lambda.Function = self._get_lambda(
            f"{usecase_name}-processed-lambda",
            f"{usecase_name}_processed",
            lambda_role,
            self.get_lambda_script_location(),
            f"nwm_transform_stream.lambda_function.{usecase_name}_processed_lambda_handler",
            memory_size=512,
        )

        # setup processed lambda
        self._add_lambda_variables(lambda_function)

        lambda_function.add_event_source(
            aws_lambda_event_sources.KinesisEventSource(
                stream=aws_kinesis.Stream.from_stream_arn(
                    self,
                    f"{usecase_name}-kinesis-processed-stream-trigger",
                    source_kinesis_stream_arn,
                ),
                max_record_age=Duration.days(1),
                bisect_batch_on_error=True,
                batch_size=self.processed_config.kinesis_batch_size,
                parallelization_factor=self.processed_config.kinesis_parallelization_factor,
                max_batching_window=Duration.seconds(10),
                starting_position=aws_lambda.StartingPosition.LATEST,
                retry_attempts=3,
            )
        )

        self.alarmgenerator.create_lambda_function_alarms(
            self.app_ctx.name_resource(f"{usecase_name}_processed")
        )
        return lambda_function

    def _create_store_lambda(
        self,
        source_kinesis_stream_arn: str,
        usecase_name: str = "usecase",
    ) -> aws_lambda.Function:
        """
        Creates a store lambda function instance which takes the processed
        data from stream and saves it into a database table

        :param source_kinesis_stream_arn: arn of the incoming kinesis stream
        :type source_kinesis_stream_arn: str
        :param usecase_name: name of the usecase, e.g. wanlink, system1 etc, defaults to "usecase"
        :type usecase_name: str, optional
        :return: the created lambda function
        :rtype: aws_lambda.Function
        """

        lambda_role: aws_iam.Role = GenericLambdaRole(
            self, self.app_ctx, f"{usecase_name}-store-role"
        ).create_lambda_role(
            name=f"{usecase_name}_store",
            source_kinesis_stream_arn=source_kinesis_stream_arn,
            rds_cluster=self.store_config.rds_cluster,
            secret_prefix=self.store_config.rds_secrets_manager_name,
            vpc=True,
        )

        # create lambda
        lambda_function: aws_lambda.Function = self._get_lambda(
            f"{usecase_name}-store-lambda",
            f"{usecase_name}_store",
            lambda_role,
            self.get_lambda_script_location(),
            f"nwm_transform_stream.lambda_function.{usecase_name}_store_lambda_handler",
            vpc=self.vpc,
            memory_size=512,
        )

        # setup processed lambda
        self._add_lambda_variables(lambda_function)

        lambda_function.add_event_source(
            aws_lambda_event_sources.KinesisEventSource(
                stream=aws_kinesis.Stream.from_stream_arn(
                    self,
                    f"{usecase_name}-kinesis-store-stream-trigger",
                    source_kinesis_stream_arn,
                ),
                max_record_age=Duration.days(1),
                bisect_batch_on_error=True,
                batch_size=self.store_config.kinesis_batch_size,
                parallelization_factor=self.store_config.kinesis_parallelization_factor,
                max_batching_window=Duration.seconds(
                    self.store_config.kinesis_batch_window
                ),
                starting_position=aws_lambda.StartingPosition.LATEST,
            )
        )

        self.alarmgenerator.create_lambda_function_alarms(
            self.app_ctx.name_resource(f"{usecase_name}_store")
        )
        return lambda_function

    def _get_lambda(
        self,
        construct_id: str,
        lambda_name: str,
        lambda_role: aws_iam.Role,
        lambda_script_location: str,
        lambda_handler: str,
        **kwargs,
    ) -> aws_lambda.Function:
        """

        Temporary. Just for showing the new layer being added

        Get lambda function from common library

        :param construct_id: name of store, e.g. captured, curated, processed
        :param lambda_name: name of the lambda function, default: lambda-function
        :param lambda_role: lambda role from ``_get_lambda_role`` method
        :param lambda_script_location: location of the lambda script
        :param lambda_handler: method/handler for lambda function

        :return: lambda function
        """
        lambda_function = GenericLambda(
            self, self.app_ctx, self.kms, f"{construct_id}-lambda-function"
        ).create_lambda_function(
            lambda_role=lambda_role,
            name=lambda_name,
            script_location=lambda_script_location,
            handler=lambda_handler,
            runtime=aws_lambda.Runtime.PYTHON_3_9,
            timeout=Duration.minutes(10),
            **kwargs,
        )

        lambda_layer = aws_lambda.LayerVersion.from_layer_version_arn(
            self,
            f"flake-python-commons-{construct_id}",
            {
                "tst": "arn:aws:lambda:eu-central-1:246910517774:layer:flake-python-commons-layer-tst:75",
                "dev": "arn:aws:lambda:eu-central-1:246910517774:layer:flake-python-commons-layer-tst:75",
                "prd": "arn:aws:lambda:eu-central-1:988135344151:layer:flake-python-commons-layer-prd:9",
            }[self.app_ctx.env_name.value],
        )

        lambda_function.add_layers(lambda_layer)

        return lambda_function
