import setuptools


with open("../README.md") as fp:
    long_description = fp.read()


setuptools.setup(
    name="NWMTransformStreamStack",
    version="1.0.0",
    description="An empty CDK Python app",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="what",
    package_dir={"": "nwm_stream"},
    packages=setuptools.find_packages(where="nwm_stream"),
    install_requires=[
        "configparser==5.3.0",
        "ppds-cdk-lib==7.11.0",
    ],
    python_requires=">=3.8",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: Apache Software License",
        "Programming Language :: JavaScript",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Topic :: Software Development :: Code Generators",
        "Topic :: Utilities",
        "Typing :: Typed",
    ],
)
