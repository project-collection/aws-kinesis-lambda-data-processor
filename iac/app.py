import os
import aws_cdk as cdk
from ppds_cdk_lib.common.application_context import (
    EnvName,
    ApplicationName,
    ApplicationContext,
)
from nwm_stream.nwm_stream_stack import NWMTransformStreamStack

env = EnvName(os.environ["CI_ENVIRONMENT_NAME"])
ci_commit_sha = os.environ["CI_COMMIT_SHORT_SHA"]

app_ctx = ApplicationContext(
    app_name=ApplicationName.flake, job_name="nwm-stream", env_name=env
)

app = cdk.App()


stack = NWMTransformStreamStack(
    app,
    app_ctx,
    stack_name=app_ctx.name_resource("lambda-stack"),
    description="NWM-Transform-Stream",
    env=cdk.Environment(
        region="eu-central-1", account=os.environ["AWS_ACCOUNT_NUMBER"]
    ),
    dbs_cost_reference=os.environ["COST_REFERENCE"],
    cost_monitoring_tag="nwm",
)


app.synth()
