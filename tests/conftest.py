import pytest
from pathlib import Path
import json
import base64
from typing import List, Dict
from dataclasses import dataclass
from aws_lambda_powertools.utilities.data_classes.kinesis_stream_event import (
    KinesisStreamRecord,
    KinesisStreamEvent,
    KinesisStreamRecordPayload,
)
from nwm_transform_stream.processed.config import Switch
from nwm_transform_stream.processed.config import ProcessedConfig
from nwm_transform_stream.store.config import StoreConfig
from nwm_transform_stream.common.utils import load_config, load_schema


PROCESSED_CONFIG_PATH = "src/nwm_transform_stream/resources/conf/processed/dev.conf"
STORE_CONFIG_PATH = "src/nwm_transform_stream/resources/conf/store/dev.conf"


@pytest.fixture()
def mock_env_var(monkeypatch):
    monkeypatch.setenv("CI_ENVIRONMENT_NAME", "dev")
    monkeypatch.setenv("CI_COMMIT_SHORT_SHA", "dummy")


@pytest.fixture()
def processed_conf() -> ProcessedConfig:
    return load_config("dev", path=PROCESSED_CONFIG_PATH, config_type=ProcessedConfig)


@pytest.fixture()
def store_conf() -> StoreConfig:
    return load_config("dev", path=STORE_CONFIG_PATH, config_type=StoreConfig)


@pytest.fixture
def lambda_context():
    @dataclass
    class LambdaContext:
        function_name: str = "test"
        memory_limit_in_mb: int = 128
        invoked_function_arn: str = "arn:aws:lambda:eu-west-1:809313241:function:test"
        aws_request_id: str = "52fdfc07-2182-154f-163f-5f0f9a621d72"

    return LambdaContext()


def load_payload(path) -> Dict:
    with open(path, encoding="utf-8") as f:
        payload = json.loads(f.read())
    return payload


def generate_data_stream_event_record(data_payload: dict):
    """Generate Kinesis data stream record from data payload"""
    return KinesisStreamRecord(
        {
            "kinesis": KinesisStreamRecordPayload(
                {
                    "kinesisSchemaVersion": "1.0",
                    "partitionKey": "1",
                    "sequenceNumber": "49590338271490256608559692538361571095921575989136588898",
                    "data": base64.b64encode(json.dumps(data_payload).encode()),
                    "approximateArrivalTimestamp": 1545084650.987,
                }
            ),
            "eventSource": "aws:kinesis",
            "eventVersion": "1.0",
            "eventID": "shardId-000000000006:49590338271490256608559692538361571095921575989136588898",
            "eventName": "aws:kinesis:record",
            "invokeIdentityArn": "arn:aws:iam::123456789012:role/lambda-role",
            "awsRegion": "eu-central-1",
            "eventSourceARN": "arn:aws:kinesis:us-east-2:123456789012:stream/lambda-stream",
        }
    )


def generate_kinesis_event(records: List[KinesisStreamRecord]) -> KinesisStreamEvent:
    return KinesisStreamEvent({"Records": records})


####################################################################################################
# switch_valid_kds_record_v1.9.1.json
####################################################################################################


@pytest.fixture
def json_schema_switch_v191():
    return load_schema("resources/schema/switch-v1.9.1.json")


####################################################################################################
# switch_valid_kds_record_v1.9.1.json
####################################################################################################
@pytest.fixture
def kinesis_record_switch_valid_kds_v191_event_payload() -> dict:
    return load_payload(
        Path(__file__).parent
        / "resource"
        / "kinesis_record_switch_valid_kds_record_v1.9.1.json"
    )


@pytest.fixture
def switch_valid_kds_v191_event_payload() -> dict:
    return load_payload(
        Path(__file__).parent / "resource" / "switch_valid_kds_record_v1.9.1.json"
    )


@pytest.fixture
def switch_valid_kds_v191_event_record(switch_valid_kds_v191_event_payload):
    return generate_data_stream_event_record(switch_valid_kds_v191_event_payload)


@pytest.fixture
def switch_valid_kds_v191_event(switch_valid_kds_v191_event_record):
    records = [switch_valid_kds_v191_event_record]
    return generate_kinesis_event(records)


# processed


@pytest.fixture
def processed_switch_valid_kds_v191_event_payload():
    path_to_valid_processed_kinesis_data_stream_payload = (
        Path(__file__).parent
        / "resource"
        / "valid_processed_kinesis_data_stream_record_data.json"
    )
    with open(
        path_to_valid_processed_kinesis_data_stream_payload, encoding="utf-8"
    ) as f:
        payload = json.loads(f.read())
    return payload


@pytest.fixture
def processed_switch_valid_kds_v191_event_record(
    processed_switch_valid_kds_v191_event_payload,
):
    return generate_data_stream_event_record(
        processed_switch_valid_kds_v191_event_payload
    )


@pytest.fixture
def processed_switch_valid_kds_v191_event(
    processed_switch_valid_kds_v191_event_record,
):
    records = [processed_switch_valid_kds_v191_event_record]
    return generate_kinesis_event(records)


####################################################################################################
# switch_invalid_kds_record_v1.9.1_error.json
####################################################################################################
@pytest.fixture
def switch_invalid_kds_record_v191_error_payload() -> dict:
    return load_payload(
        Path(__file__).parent
        / "resource"
        / "switch_invalid_kds_record_v1.9.1_error.json"
    )


@pytest.fixture
def switch_invalid_kds_record_v191_error_record(
    switch_invalid_kds_record_v191_error_payload,
):
    return generate_data_stream_event_record(
        switch_invalid_kds_record_v191_error_payload
    )


@pytest.fixture
def switch_invalid_kds_record_v191_error_event(
    switch_invalid_kds_record_v191_error_record,
):
    records = [switch_invalid_kds_record_v191_error_record]
    return generate_kinesis_event(records)


####################################################################################################
# switch_invalid_kds_record_v1.9.1_2.json
####################################################################################################


@pytest.fixture
def switch_invalid_kds_v191_2_event_payload() -> dict:
    return load_payload(
        Path(__file__).parent / "resource" / "switch_invalid_kds_record_v1.9.1_2.json"
    )


@pytest.fixture
def switch_invalid_kds_v191_2_event_record(switch_invalid_kds_v191_2_event_payload):
    return generate_data_stream_event_record(switch_invalid_kds_v191_2_event_payload)


@pytest.fixture
def switch_invalid_kds_v191_2_event(switch_invalid_kds_v191_2_event_record):
    records = [switch_invalid_kds_v191_2_event_record]
    return generate_kinesis_event(records)


####################################################################################################
# Result set
####################################################################################################


@pytest.fixture
def dict_record_1():
    return {
        "verbundkennung": "ICE0189",
        "data_ip": "192.168.2.179",
        "data_timestamp": "09/08/2023 11:16:23",
        "data_mac_address": "28:60:46:A0:81:1D",
        "data_management_address": "172.17.4.1",
    }


@pytest.fixture
def switch_record_1(dict_record_1):
    return Switch.parse_obj(dict_record_1)


@pytest.fixture
def processed_messages(switch_record_1) -> List:
    messages = [
        (
            "success",
            switch_record_1,
            "dummy",
        )
    ]
    return messages
