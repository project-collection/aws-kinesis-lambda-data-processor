import json
import os
import time
import boto3
import pytest
from sqlalchemy import text
from nwm_transform_stream.store.steps import get_pg_engine


boto3.setup_default_session(region_name="eu-central-1")


@pytest.fixture(scope="module")
def sha() -> str:
    return os.environ["CI_COMMIT_SHORT_SHA"].lower()


@pytest.fixture(scope="module")
def kinesis_client(processed_conf):
    return boto3.client("kinesis", region_name=processed_conf.aws_region)


@pytest.fixture(scope="module")
def ingestion_stream_switch_name(sha) -> str:
    # represents ingestion kinesis data stream for switch
    return f"flake-nwm-stream-ingestion-dev-{sha}"


@pytest.fixture(scope="module")
def put_kinesis_event_switch(
    kinesis_client,
    processed_conf,
    payload,
    ingestion_stream_switch_name,
):
    kinesis_client = boto3.client("kinesis", region_name=processed_conf.aws_region)
    print("put_kinesis_event: ")

    response = kinesis_client.put_record(
        StreamName=ingestion_stream_switch_name,
        Data=json.dumps(payload).encode("utf-8"),
        PartitionKey="0",
    )
    print(response)


def _get_kinesis_records(kinesis_client, stream_name, limit=100) -> list[dict]:
    des = kinesis_client.describe_stream(StreamName=stream_name)
    shard_info = des["StreamDescription"]["Shards"][0]

    response = kinesis_client.get_shard_iterator(
        StreamName=stream_name,
        ShardId=shard_info["ShardId"],
        ShardIteratorType="TRIM_HORIZON",
    )
    res = kinesis_client.get_records(
        ShardIterator=response["ShardIterator"], Limit=limit
    )

    data = []
    for r in res["Records"]:
        data.append(json.loads(r["Data"]))

    return data


def test_rds_delivery(store_conf):
    time.sleep(10)
    with get_pg_engine(store_conf).connect() as conn:
        sql_switch: str = f'SELECT count(*) FROM "{store_conf.rds_schema}"."{store_conf.rds_table_t_switch_name}"'
        res_switch = conn.execute(text(sql_switch)).fetchone()

        assert len(res_switch) == 1

        delete_sql_switch: str = f'DELETE FROM "{store_conf.rds_schema}"."{store_conf.rds_table_t_switch_name}"'
        conn.execute(text(delete_sql_switch))
        conn.commit()
