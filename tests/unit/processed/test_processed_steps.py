def test_transform_switch(
    switch_valid_kds_v191_event_payload, switch_record_1, mock_env_var
):
    from nwm_transform_stream.processed.app import transform_switch

    result = transform_switch(switch_valid_kds_v191_event_payload)
    expected_result = switch_record_1
    assert result == expected_result


def test_get_successfully_processed_messages(
    processed_messages, switch_record_1, mock_env_var
):
    from nwm_transform_stream.processed.app import get_successfully_processed_messages

    result = get_successfully_processed_messages(processed_messages)
    expected_result = [switch_record_1]

    assert result == expected_result


from datetime import datetime


def test_parse_data_timestamp():
    from nwm_transform_stream.processed.config import parse_date_timestamp

    s = "30/07/2023 18:50:13"
    result = parse_date_timestamp(s)
    expected_result = datetime(2023, 7, 30, 18, 50, 13)
    assert result == expected_result
