def test_switch_processed_run(
    switch_valid_kds_v191_event, lambda_context, monkeypatch, mock_env_var
):
    from nwm_transform_stream.processed.app import switch_processed_run
    import nwm_transform_stream.processed.app

    monkeypatch.setattr(
        nwm_transform_stream.processed.app, "write", lambda x, y: print("patched")
    )
    switch_processed_run(switch_valid_kds_v191_event, lambda_context)


def test_run_batch_process(switch_valid_kds_v191_event, switch_record_1, mock_env_var):
    from nwm_transform_stream.processed.app import run_batch_process

    processed_messages = run_batch_process(switch_valid_kds_v191_event)

    NUMBER_OF_SUCCESSFULLY_PROCESSED_MESSAGES = 1
    # only NUMBER_OF_SUCCESSFULLY_PROCESSED_MESSAGES with status success
    assert (
        len([msg for msg in processed_messages if msg[0] == "success"])
        == NUMBER_OF_SUCCESSFULLY_PROCESSED_MESSAGES
    )
    # check result of successfully processed messages
    results = [msg[1] for msg in processed_messages if msg[0] == "success"]
    expected_results = [switch_record_1]
    assert results == expected_results


def test_record_handler(
    switch_valid_kds_v191_event_record, switch_record_1, mock_env_var
):
    # import record_handler under patched environment variable mock_env_var
    from nwm_transform_stream.processed.app import record_handler

    res = record_handler(switch_valid_kds_v191_event_record)
    expected_res = switch_record_1
    print(res)
    print(switch_record_1)

    assert res == expected_res
