def test_switch_store_run(
    processed_switch_valid_kds_v191_event, lambda_context, monkeypatch, mock_env_var
):
    monkeypatch.setattr(
        "nwm_transform_stream.store.steps.write_to_postgres",
        lambda df, config, sort_columns, deduplication_keys, time_column, schema_name, table_name, write_method: print(
            "patched"
        ),
    )

    monkeypatch.setattr(
        "nwm_transform_stream.common.writers.get_secret_credentials",
        lambda secret_name: ("dummy", "dummy"),
    )

    from nwm_transform_stream.store.app import switch_store_run

    switch_store_run(processed_switch_valid_kds_v191_event, lambda_context)
