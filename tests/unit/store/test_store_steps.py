
def test_write_t_switch(mock_env_var, monkeypatch, store_conf, dict_record_1):
    monkeypatch.setenv("CI_ENVIRONMENT_NAME", "dev")
    monkeypatch.setenv("CI_COMMIT_SHORT_SHA", "dummy")
    import nwm_transform_stream
    from nwm_transform_stream.store.steps import write_t_switch

    monkeypatch.setattr(
        "nwm_transform_stream.store.steps.get_pg_engine",
        lambda x1: "dummy",
    )
    monkeypatch.setattr(
        nwm_transform_stream.store.steps.PostgresWriter,
        "__init__",
        lambda x1, x2, x3: None,
    )
    monkeypatch.setattr(
        nwm_transform_stream.store.steps.PostgresWriter,
        "drop_duplicates",
        lambda data, keys, time_column: "dummy",
    )
    monkeypatch.setattr(
        nwm_transform_stream.store.steps.PostgresWriter,
        "write",
        lambda x1, x2: None,
    )

    write_t_switch(records=[dict_record_1], config=store_conf)
