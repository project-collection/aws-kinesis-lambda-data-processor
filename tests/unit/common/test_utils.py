from nwm_transform_stream.common.utils import (
    validate_schema,
    extract_from_kinesis_stream,
)


def test_validate_schema_valid(switch_valid_kds_v191_event_payload):
    json_data = switch_valid_kds_v191_event_payload
    res = validate_schema(json_data)
    assert res == True


def test_validate_schema_invalid(switch_invalid_kds_v191_2_event_payload):
    json_data = switch_invalid_kds_v191_2_event_payload
    res = validate_schema(json_data)
    assert res == False


def test_extract_from_kinesis_stream_valid(
    switch_valid_kds_v191_event, switch_valid_kds_v191_event_record
):
    records, corrupt_counter = extract_from_kinesis_stream(switch_valid_kds_v191_event)
    assert corrupt_counter == 0
    assert len(records) == 1
    assert (
        records[0].kinesis.data_as_text()
        == switch_valid_kds_v191_event_record.kinesis.data_as_text()
    )


def test_extract_from_kinesis_stream_invalid(switch_invalid_kds_v191_2_event):
    records, corrupt_counter = extract_from_kinesis_stream(
        switch_invalid_kds_v191_2_event
    )
    assert corrupt_counter == 1
    assert len(records) == 0
