import pytest
import json
from nwm_transform_stream import lambda_function
from aws_lambda_powertools import Metrics
from aws_lambda_powertools.metrics import metrics as metrics_global


@pytest.fixture(scope="function", autouse=True)
def reset_metric_set():
    # Clear out every metric data prior to every test
    metrics = Metrics()
    metrics.clear_metrics()
    metrics_global.is_cold_start = True  # ensure each test has cold start
    metrics.clear_default_dimensions()  # remove persisted default dimensions, if any
    yield


def test_log_metrics_switch_processed_app(
    switch_valid_kds_v191_event, capsys, lambda_context, monkeypatch, mock_env_var
):
    import nwm_transform_stream.processed.app

    monkeypatch.setattr(
        nwm_transform_stream.processed.app, "write", lambda x, y: print("")
    )

    lambda_function.switch_processed_lambda_handler(
        dict(switch_valid_kds_v191_event), lambda_context
    )

    log = capsys.readouterr().out.strip()  # remove any extra line
    metrics_output = json.loads(log)  # deserialize JSON str
    metric_names = [
        m["Name"] for m in metrics_output["_aws"]["CloudWatchMetrics"][0]["Metrics"]
    ]

    # THEN we should have no exceptions
    # and a valid EMF object should be flushed correctly
    assert (
        "InvalidRecordsCountSwitchProcessed" in log
    )  # basic string assertion in JSON str
    assert "InvalidRecordsCountSwitchProcessed" in metric_names

    assert "InputRecordsCountSwitchProcessed" in log
    assert "InputRecordsCountSwitchProcessed" in metric_names

    assert "OutputRecordsCountSwitchProcessed" in log
    assert "OutputRecordsCountSwitchProcessed" in metric_names


def test_log_metrics_switch_store_app(
    processed_switch_valid_kds_v191_event,
    capsys,
    lambda_context,
    monkeypatch,
    mock_env_var,
):
    monkeypatch.setattr(
        "nwm_transform_stream.store.steps.write_to_postgres",
        lambda df, config, sort_columns, deduplication_keys, time_column, schema_name, table_name, write_method: print(
            ""
        ),
    )
    monkeypatch.setattr(
        "nwm_transform_stream.common.writers.get_secret_credentials",
        lambda secret_name: ("dummy", "dummy"),
    )

    lambda_function.switch_store_lambda_handler(
        dict(processed_switch_valid_kds_v191_event), lambda_context
    )

    log = capsys.readouterr().out.strip()  # remove any extra line
    metrics_output = json.loads(log)  # deserialize JSON str
    metric_names = [
        m["Name"] for m in metrics_output["_aws"]["CloudWatchMetrics"][0]["Metrics"]
    ]

    assert "InputRecordsCountSwitchStore" in log
    assert "InputRecordsCountSwitchStore" in metric_names
