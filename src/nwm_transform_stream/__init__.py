#!/usr/bin/env python3

# Copyright (c) 2023 DB Fernverkehr AG
# License: DBISL, see the accompanying file LICENSE


from pathlib import Path


def get_project_root() -> Path:
    return Path(__file__).parent
