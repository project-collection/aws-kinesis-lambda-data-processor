#!/usr/bin/env python3

# Copyright (c) 2023 DB Fernverkehr AG
# License: DBISL, see the accompanying file LICENSE

import os
import pandas as pd
from aws_lambda_powertools import Logger
from nwm_transform_stream.store.config import StoreConfig
from nwm_transform_stream.common.writers import PostgresWriter, get_pg_engine
from nwm_transform_stream.processed.config import Switch
from flake_python_commons.configs import PostgresWriterConfig, PostgresWriteMethod
from typing import List

logger = Logger()


def write_to_postgres(
    df: pd.DataFrame,
    config: StoreConfig,
    sort_columns: List[str],
    deduplication_keys: List[str],
    time_column: str,
    schema_name: str,
    table_name: str,
    write_method,
):
    engine = get_pg_engine(config)

    writer_config = PostgresWriterConfig(
        app_name=config.cloud_watch_dim["AppName"],
        environment=os.environ["CI_ENVIRONMENT_NAME"],
        postgres_table_name=table_name,
        postgres_schema=schema_name,
        write_method=write_method,
        dataframe_sort_columns=sort_columns,
    )
    writer = PostgresWriter(writer_config, engine)
    # deduplication before writing to database
    df = PostgresWriter.drop_duplicates(
        data=df, keys=deduplication_keys, time_column=time_column
    )
    try:
        writer.write(df)
    except Exception as e:
        logger.debug("Error while writing to postgres with schema: " + str(df.dtypes))
        logger.debug("Dataframe: " + str(df))
        raise e


def enforce_schema_switch(df):
    """Fix the schema of the dataframe before writing to the target system."""
    df["verbundkennung"] = df["verbundkennung"].astype("string")
    df["data_ip"] = df["data_ip"].astype("string")
    df["data_mac_address"] = df["data_mac_address"].astype("string")
    df["data_management_address"] = df["data_management_address"].astype("string")
    df["data_timestamp"] = pd.to_datetime(
        df["data_timestamp"], format="%d/%m/%Y %H:%M:%S"
    )
    return df


def write_t_switch(records: list[dict], config: StoreConfig):
    """
    Update records in postgres
    Do not close the connection to the database. Just use it.
    """

    # parsing using model switch
    converted_records = [Switch.parse_obj(i) for i in records]
    df = pd.DataFrame([r.dict() for r in converted_records])
    # Schema conversion needs to be enforced here. See DB Schema for more information.
    df = enforce_schema_switch(df)
    write_to_postgres(
        df=df,
        config=config,
        sort_columns=["data_timestamp"],
        deduplication_keys=["data_mac_address"],
        time_column="data_timestamp",
        schema_name=config.rds_schema,
        table_name=config.rds_table_t_switch_name,
        write_method=PostgresWriteMethod.upsert,
    )
