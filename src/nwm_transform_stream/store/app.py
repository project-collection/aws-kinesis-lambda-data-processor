# Copyright (c) 2023 DB Fernverkehr AG
# License: DBISL, see the accompanying file LICENSE


import os
from aws_lambda_powertools import Metrics, Logger
from aws_lambda_powertools.metrics import MetricUnit
from aws_lambda_powertools.utilities.data_classes import KinesisStreamEvent
from typing import List, Dict
from nwm_transform_stream.store.config import StoreConfig
from nwm_transform_stream.common.config import MetricNames
from nwm_transform_stream.common.utils import load_config
from nwm_transform_stream.store.steps import write_t_switch
from nwm_transform_stream import get_project_root


logger = Logger()
env = os.environ["CI_ENVIRONMENT_NAME"]
config_path = os.path.join(
    get_project_root(), "resources", "conf", "store", f"{env.lower()}.conf"
)
conf: StoreConfig = load_config(env, path=config_path, config_type=StoreConfig)
metrics = Metrics(namespace=conf.cloud_watch_ns)
metrics.set_default_dimensions(**conf.cloud_watch_dim)


@metrics.log_metrics
def switch_store_run(event: KinesisStreamEvent, context) -> None:
    logger.info("Start lambda execution.")
    # extract payloads from stream
    records: List[Dict] = [r.kinesis.data_as_json() for r in event.records]
    logger.info(f"Loaded {len(records)} records with schema from Kinesis")
    metrics.add_metric(
        MetricNames.InputputRecordsCountSwitchStore, MetricUnit.Count, len(records)
    )
    write_t_switch(records, conf)
    logger.info("Stopping lambda execution.")
