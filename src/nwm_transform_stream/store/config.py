# Copyright (c) 2023 DB Fernverkehr AG
# License: DBISL, see the accompanying file LICENSE


from pydantic import Extra, Field
from nwm_transform_stream.common.config import KinesisConfig


class StoreConfig(KinesisConfig):
    # aws default region
    aws_region: str = "eu-central-1"
    # cw namespace
    cloud_watch_ns: str
    # cw metric dimensions
    cloud_watch_dim: dict

    rds_cluster: str = Field(
        ...,
        description="RDS cluster host",
    )
    rds_database: str = Field(..., description="RDS Database Name")
    rds_schema: str = Field(..., description="RDS Schema to create / Write tables")
    rds_table_t_switch_name: str = Field(
        "t_switch",
        description="RDS Table to map t_switch, this is not hardcoded because of integration tests ",
    )
    rds_secrets_manager_name: str = Field(..., description="AWS Secret Manager name")

    def __hash__(self) -> int:
        return (
            self.aws_region
            + self.rds_cluster
            + self.rds_database
            + self.rds_schema
            + self.rds_table_t_wanlinkradio_name
            + self.rds_table_t_wanlinknetwork_name
            + self.rds_table_t_wificard_name
        ).__hash__()
