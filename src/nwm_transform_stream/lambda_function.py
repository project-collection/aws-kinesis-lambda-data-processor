#!/usr/bin/env python3

# Copyright (c) 2023 DB Fernverkehr AG
# License: DBISL, see the accompanying file LICENSE


from aws_lambda_powertools import Logger, Tracer
from aws_lambda_powertools.utilities.data_classes import (
    KinesisStreamEvent,
    event_source,
)

logger = Logger()


@event_source(data_class=KinesisStreamEvent)
def switch_processed_lambda_handler(event: KinesisStreamEvent, context):
    from nwm_transform_stream.processed.app import switch_processed_run

    logger.debug(event)
    switch_processed_run(event, context)


@event_source(data_class=KinesisStreamEvent)
def switch_store_lambda_handler(event: KinesisStreamEvent, context):
    from nwm_transform_stream.store.app import switch_store_run

    logger.debug(event)
    switch_store_run(event, context)
