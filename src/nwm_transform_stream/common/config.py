from pydantic import BaseSettings, Extra


class KinesisConfig(BaseSettings):
    # kinesis subscription batch window / seconds
    kinesis_batch_window: int = 30
    # kinesis subscription batch size
    kinesis_batch_size: int = 2500
    kinesis_parallelization_factor = 4

    class Config:
        extra = Extra.allow


class MetricNames:
    InputRecordsCountSwitchProcessed: str = "InputRecordsCountSwitchProcessed"
    OutputRecordsCountSwitchProcessed: str = "OutputRecordsCountSwitchProcessed"
    InputputRecordsCountSwitchStore: str = "InputRecordsCountSwitchStore"
    InvalidRecordsCountSwitchProcessed: str = "InvalidRecordsCountSwitchProcessed"
