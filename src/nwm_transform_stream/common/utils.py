from aws_lambda_powertools import Logger
from aws_lambda_powertools.utilities.data_classes.kinesis_stream_event import (
    KinesisStreamEvent,
    KinesisStreamRecord,
)
import boto3
import functools
from pyhocon import ConfigFactory
from typing import Union
import os
import json
from dataclasses import dataclass
from nwm_transform_stream.common.config import KinesisConfig
from nwm_transform_stream.store.config import StoreConfig
from nwm_transform_stream import get_project_root
from nwm_transform_stream.processed.config import ProcessedConfig
import fastjsonschema
from typing import Tuple, List

logger = Logger()


@dataclass
class Config:
    rds_secrets_manager_name: str
    rds_cluster: str
    rds_database: str


def get_secret_credentials(secret_name: str):
    """
    Get user and password of the given AWS Secret
    :param secret_name: the name of the Secret whose values should be retrieved
    :return: the username and password of the given AWS Secret
    """
    client = boto3.client("secretsmanager", region_name="eu-central-1")
    credentials = json.loads(
        client.get_secret_value(SecretId=secret_name)["SecretString"]
    )
    return credentials["user"], credentials["password"]


@functools.lru_cache(maxsize=6)
def load_config(
    env: str, path: str, config_type: Union[ProcessedConfig, StoreConfig]
) -> KinesisConfig:
    conf = ConfigFactory.parse_file(path)
    return config_type.parse_obj(conf)


@functools.lru_cache(maxsize=4)
def load_schema(filename: str, root_path=get_project_root()) -> dict:
    """
    Load a json schema
    """
    filepath = os.path.join(root_path, *(filename.split("/")))
    logger.debug(f"Loading json schema {filepath}")

    with open(filepath, encoding="utf-8") as f:
        schema = json.load(f)

    return schema


def validate_schema(record: dict) -> Tuple[dict, bool]:
    valid_format_versions = ["1.6.1", "1.9.1"]
    is_valid = True

    logger.debug(f"Got event: {record}")
    try:
        # only process switch data
        if record["type"] != "com.deutschebahn.dbfv.nwm.switch":
            is_valid = False
        # it is switch data, begins with validating
        else:
            json_data = record["data"]
            format_version = json_data["formatVersion"]
            if format_version not in valid_format_versions:
                logger.warning(
                    f"Found unrecognized record format version: {format_version}"
                )
                raise ValueError(f"format version {format_version} is not recognized")
            else:
                schema_path = f"resources/schema/switch-v{format_version}.json"
                json_schema = load_schema(schema_path)
                validate = fastjsonschema.compile(json_schema)
                validate(json_data)
    except Exception as e:
        is_valid = False
        logger.warning(
            f"Found corrupt record: {json_data}: validationError: {e.message}"
        )
    return is_valid


def extract_from_kinesis_stream(
    event: KinesisStreamEvent,
) -> Tuple[List[KinesisStreamRecord], int]:
    """Extract and validate payload from KinesisStreamEvent.
    which schema version to be used is decided on-the-fly in validate_schema function
    returns: a list of dict with the values and as int the number of corrupted records
    """
    records = []
    corrupt_counter = 0
    for r in event.records:
        record = r.kinesis.data_as_json()
        if validate_schema(record):
            records.append(r)
        else:
            corrupt_counter += 1

    return records, corrupt_counter
