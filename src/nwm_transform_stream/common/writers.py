import boto3
import json
from datetime import datetime
from typing import Union, List, Dict
from sqlalchemy import Engine, text
from flake_python_commons.writers.postgres_writer import PostgresWriter
from nwm_transform_stream.store.config import StoreConfig
from nwm_transform_stream.processed.config import ProcessedConfig
from nwm_transform_stream.common.utils import get_secret_credentials


def put_to_kinesis(data: list, aws_region, stream_name, partition_key_column):
    """
    Write data to kinesis data stream

    :param partition_key_column: The column to use as partition key, extract from data
    :param stream_name: kinesis stream name
    :param data: data
    :param aws_region: aws region, default: eu-central-1
    """
    client = boto3.client("kinesis", region_name=aws_region)
    records = []
    default_part = datetime.utcnow().strftime("%Y-%m-%d")
    for o in data:
        records.append(
            {
                "Data": (json.dumps(o)).encode("utf-8"),
                "PartitionKey": f"{o.get(partition_key_column, default_part)}",
            }
        )

    if len(records) > 0:
        client.put_records(StreamName=stream_name, Records=records)

    print(f"Successfully wrote {stream_name} {len(records)} records to kinesis")


def batch(iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx : min(ndx + n, l)]


def write(records: List[Dict], config: Union[ProcessedConfig, StoreConfig]):
    """
    Write records, to kinesis and firehose
    """
    # convert BaseModels to dict
    record_dict = [record.dict() for record in records]
    # AWS Kinesis has a limit of 500 records per put
    for x in batch(record_dict, 500):
        put_to_kinesis(
            x,
            config.aws_region,
            config.switch_kinesis_out.kinesis_name,
            config.switch_kinesis_out.kinesis_partition_key,
        )


def get_pg_engine(config) -> Engine:
    # Credentials
    username, password = get_secret_credentials(
        secret_name=config.rds_secrets_manager_name
    )
    engine = PostgresWriter.get_engine(
        f"postgresql+psycopg2://{username}:{password}@{config.rds_cluster}/{config.rds_database}?sslmode=require"
    )

    with engine.connect() as conn:
        result = conn.execute(text("select 'TEST'"))
        result.fetchone()

    return engine
