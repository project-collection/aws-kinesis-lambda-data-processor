from nwm_transform_stream.common.config import KinesisConfig
from pydantic import BaseModel, validator
from typing import Optional
from datetime import datetime


class KinesisOutput(BaseModel):
    kinesis_partition_key: str
    kinesis_name: str


class KinesisInput(BaseModel):
    schema_path: str
    kinesis_source_arn: str


class ProcessedConfig(KinesisConfig):
    """
    Json Schema file path relative to ``<project_root>``
    Example:  or ``resources/processed/schema/input__v1_0.json``
    """

    # aws default region
    aws_region: str = "eu-central-1"
    # cw namespace
    cloud_watch_ns: str
    # cw metric dimensions
    cloud_watch_dim: dict
    # input config switch
    switch_kinesis_input: KinesisInput
    # output config switch
    switch_kinesis_out: KinesisOutput


def parse_date_timestamp(data_timestamp: str):
    try:
        data_timestamp = datetime.strptime(data_timestamp, "%d/%m/%Y %H:%M:%S")
        return data_timestamp
    except ValueError:
        raise ValueError("Invalid timestamp format")


class Switch(BaseModel):
    verbundkennung: str
    data_ip: str
    data_timestamp: str
    data_mac_address: str
    data_management_address: str

    @validator("data_timestamp")
    def parse_timestamp(cls, value):
        # try parsing
        parse_date_timestamp(value)
        return value
