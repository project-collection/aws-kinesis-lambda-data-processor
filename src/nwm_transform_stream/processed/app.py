import os
from aws_lambda_powertools import Logger, Metrics
from aws_lambda_powertools.metrics import MetricUnit
from aws_lambda_powertools.utilities.data_classes import KinesisStreamEvent
from aws_lambda_powertools.utilities.data_classes.kinesis_stream_event import (
    KinesisStreamRecord,
)
from aws_lambda_powertools.utilities.batch import BatchProcessor, EventType
from aws_lambda_powertools.utilities.batch.exceptions import BatchProcessingError
from nwm_transform_stream.processed.config import ProcessedConfig, Switch
from nwm_transform_stream.processed.steps import (
    transform_switch,
    get_successfully_processed_messages,
)
from nwm_transform_stream.common.utils import (
    load_config,
    load_schema,
    extract_from_kinesis_stream,
)
from nwm_transform_stream.common.config import MetricNames
from nwm_transform_stream.common.writers import write
from nwm_transform_stream import get_project_root
from typing import List, Tuple


env = os.environ["CI_ENVIRONMENT_NAME"]
config_path = os.path.join(
    get_project_root(), "resources", "conf", "processed", f"{env.lower()}.conf"
)

conf: ProcessedConfig = load_config(
    env=env, path=config_path, config_type=ProcessedConfig
)
logger = Logger()
processor = BatchProcessor(event_type=EventType.KinesisDataStreams)
metrics_engine = Metrics(namespace=conf.cloud_watch_ns)
metrics_engine.set_default_dimensions(**conf.cloud_watch_dim)


def record_handler(record: KinesisStreamRecord):
    """Process logic of a single kinesis data record"""
    payload: dict = record.kinesis.data_as_json()
    try:
        processed_switch_record: Switch = transform_switch(payload)
    except Exception as e:
        logger.info(e)
        logger.debug(payload)
        raise e

    return processed_switch_record


def run_batch_process(event: KinesisStreamEvent):
    """Batch processing on kinesis data stream event, list of records"""
    # schema validation
    records, corrupt_counter = extract_from_kinesis_stream(event)

    with processor(records=records, handler=record_handler):
        processed_messages: List[Tuple] = processor.process()

    metrics_engine.add_metric(
        MetricNames.OutputRecordsCountSwitchProcessed,
        MetricUnit.Count,
        len(processed_messages),
    )
    metrics_engine.add_metric(
        MetricNames.InvalidRecordsCountSwitchProcessed,
        MetricUnit.Count,
        corrupt_counter,
    )
    metrics_engine.add_metric(
        MetricNames.InputRecordsCountSwitchProcessed,
        MetricUnit.Count,
        len(processed_messages) + corrupt_counter,
    )
    logger.info(
        f"Successfully processed ${len(processed_messages)} records. Number of corrupted records: {corrupt_counter}"
    )
    return processed_messages


@metrics_engine.log_metrics
def switch_processed_run(event: KinesisStreamEvent, context):
    """Main function to processed lambda on "switch" data"""
    processed_messages: List[Tuple] = run_batch_process(event)
    try:
        result_t_switch: List[Switch] = get_successfully_processed_messages(
            processed_messages
        )
        # write to kinesis stream
        write(result_t_switch, conf)
    except:
        raise BatchProcessingError
    return processor.response()
