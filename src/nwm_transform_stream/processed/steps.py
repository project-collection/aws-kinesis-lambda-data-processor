import pandas as pd
from typing import Dict, List
from nwm_transform_stream.processed.config import Switch


def transform_switch(payload: Dict) -> Switch:
    # REPLACE(subject, 'TZ', 'ICE')
    verbundkennung = payload["subject"]
    verbundkennung = verbundkennung.replace("TZ", "ICE")
    data_ip = payload["data"]["ip"]
    data_timestamp = payload["data"]["timestamp"]
    data_mac_address = payload["data"]["macAddressInfo"]
    data_management_address = payload["data"]["managementAddress"]
    return Switch(
        verbundkennung=verbundkennung,
        data_ip=data_ip,
        data_timestamp=data_timestamp,
        data_mac_address=data_mac_address,
        data_management_address=data_management_address,
    )


def get_successfully_processed_messages(processed_messages: List) -> pd.DataFrame:
    successfully_processed_messages = [
        msg[1] for msg in processed_messages if msg[0] == "success"
    ]

    return successfully_processed_messages
